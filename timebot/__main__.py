import json, asyncio, datetime
import discord

with open('config.json') as config_file:
    config = json.load(config_file)

client = discord.Client()

async def send_messages():
    await client.wait_until_ready()

    previous_timestamp = None

    while not client.is_closed():
        timestamp = datetime.datetime.now()

        if previous_timestamp == None or (previous_timestamp.hour != timestamp.hour or previous_timestamp.minute != timestamp.minute):
            for message in config['messages']:
                if timestamp.hour == message['hour'] and timestamp.minute == message['minute']:
                    for channel in message['channels']:
                        await client.get_channel(channel).send(message['message'])
            
            previous_timestamp = timestamp

        await asyncio.sleep(1)

client.loop.create_task(send_messages())

client.run(config['token'])
